# webtail

One of many implementation of tailing a logfile and present on web.

Written as an experiment of using websockets in go.

![screenshot](demo.png)


## Install

 go get gitlab.com/kjell/webtail

## Usage

 
 webtail -file /var/log/somefile.log
 
 # and then, open your browser on http://localhost:8555

