// gotail tails your log and serve data to a web page.
//
// tail file given as input. Serve data over websockets.
//
// kk 2017-03

/*
Pet project.
  todos:

  - [X] - style
  - [X] - actual print file content of the file we are tailing
  - [X] - first request must get "cached data", in bucket, and after that append
          any new lines
  - [X] - handle more than one client.
  - [X] - higlight line based on keywords
*/
package main

import (
	"flag"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"github.com/hpcloud/tail"
)

var fHelp bool
var fFile string

var data chan string
var bucket []string
var c = 0

func main() {
	data = make(chan string, 10)
	flag.BoolVar(&fHelp, "help", false, "Help")
	flag.StringVar(&fFile, "file", "", "File to tail")
	flag.Parse()

	if fHelp {
		flag.Usage()
		os.Exit(0)
	}

	if fFile == "" {
		flag.Usage()
		os.Exit(0)
	}

	connectionpool = make(map[*connection]bool)

	go tailfile(fFile)

	r := mux.NewRouter()

	r.HandleFunc("/", homeHandler)
	r.HandleFunc("/ws", webSocketHandler)

	http.Handle("/", r)
	err := http.ListenAndServe(":8555", r)
	if err != nil {
		log.Fatal(err)
	}
}

// - https://github.com/gorilla/websocket/blob/master/examples/filewatch/main.go
var upgrader = websocket.Upgrader{ReadBufferSize: 1024, WriteBufferSize: 1024}
var writeWait = 10 * time.Second
var pongWait = 60 * time.Second
var pingPeriod = (pongWait * 9) / 10

func webSocketHandler(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		if _, ok := err.(websocket.HandshakeError); !ok {
			log.Println(err)
		}
		return
	}

	go writer(ws)
	reader(ws)
}

func broadcast(s string) {
	for c := range connectionpool {
		select {
		case c.send <- s:
		default:
			{
				log.Println("connection blocking. Kill it!")
				delete(connectionpool, c)
				close(c.send)
			}
		}
	}
}

type connection struct {
	send chan string
}

var connectionpool map[*connection]bool

func writer(ws *websocket.Conn) {
	err := ws.SetWriteDeadline(time.Now().Add(writeWait))
	if err != nil {
		log.Println(err)
	}
	pingTicker := time.NewTicker(pingPeriod)
	mychan := connection{make(chan string, 10)}
	connectionpool[&mychan] = true

	defer func() {
		pingTicker.Stop()
		err := ws.Close()
		if err != nil {
			log.Println(err)
		}
	}()

	// send buffer to client.
	for _, v := range bucket {
		if err := ws.WriteMessage(websocket.TextMessage, []byte(v)); err != nil {
			log.Println("Error writing data to client..", err)
		}
	}

	// listen for new lines and send to client.
	for {
		select {
		case <-pingTicker.C:
			err := ws.SetWriteDeadline(time.Now().Add(writeWait))
			if err != nil {
				log.Println(err)
			}
			if err := ws.WriteMessage(websocket.PingMessage, []byte{}); err != nil {
				return
			}

		case s, ok := <-mychan.send:
			if !ok {
				log.Println("someone closed the channel..")
				return
			}

			err := ws.SetWriteDeadline(time.Now().Add(writeWait))
			if err != nil {
				log.Println(err)
			}

			if err := ws.WriteMessage(websocket.TextMessage, []byte(fmt.Sprintf("%s", s))); err != nil {
				return
			}
		}
		time.Sleep(time.Millisecond * 100)
	}
}

func reader(ws *websocket.Conn) {
	defer func() {

		err := ws.Close()
		if err != nil {
			log.Println("Error closing ws. defered func.")
		}
	}()
	ws.SetReadLimit(512)
	if err := ws.SetReadDeadline(time.Now().Add(pongWait)); err != nil {
		log.Println("Error setting deadline", err)
	}

	ws.SetPongHandler(func(string) error {
		err := ws.SetReadDeadline(time.Now().Add(pongWait))
		if err != nil {
			log.Println(err)
		}
		return nil
	})

	for {
		_, _, err := ws.ReadMessage()
		log.Println("is this ithgt?")
		if err != nil {
			break
		}
	}
}

var homeTempl = template.Must(template.New("").Parse(homeHTML))

func homeHandler(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	var v = struct {
		Host string
		Data string
		File string
	}{
		r.Host,
		"Waiting for data...",
		fFile,
	}
	err := homeTempl.Execute(w, &v)
	if err != nil {
		log.Fatal("Error executing template..")
	}
}

func tailfile(f string) {
	t, err := tail.TailFile(f, tail.Config{Follow: true})
	if err != nil {
		log.Fatal(err)
	}

	buffersize := 40

	for line := range t.Lines {
		bucket = append(bucket, fmt.Sprintf("%d\t%s\n", c, line.Text))
		c++
		if len(bucket) > buffersize-1 {
			bucket = bucket[1:buffersize]
		}
		broadcast(bucket[len(bucket)-1])
	}
}

const homeHTML = `<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Webtail</title>
    <link rel="stylesheet" href="https://unpkg.com/awsm.css/dist/awsm.min.css">

    <style type="text/css">
      <!--
p.alert{ color: red; }
p.warning{ color: orange}
/*#fileData p{ display: inline; float left;}*/
#fileData p:hover{ background:#444;font-weight:bold}
#fileData {
    overflow:auto;
    padding:20px;
    background: black;
    height: 400px;
    width: 800px;
    font-family: monospace;
    white-space: pre-wrap;
    word-wrap: break-word;
    color: #ddd;
    line-height: 140%;
    font-size: 12px;
    cursor:default;
}

#fileData p{
    padding: 0;
    margin: 0;
}
	-->
    </style>
  </head>
  <body>
    <h2>Tailing file: <tt>{{.File}}</tt></h2>
    <div id="fileData">{{.Data}}</div>
    <script type="text/javascript">
            (function() {
                var first = true;
                var data = document.getElementById("fileData");
                var conn = new WebSocket("ws://{{.Host}}/ws");
                conn.onclose = function(evt) {
		    var dataelement = document.createElement("P")
		    dataelement.innerHTML = '## Tail ended. Connection closed';
		    data.appendChild(dataelement);
		    data.scrollTop = data.scrollHeight;
                }
                conn.onmessage = function(evt) {
                    if (first){
                        data.textContent = "";
                        first = false;
                    }
                    var dataelement = document.createElement("P")
                    dataelement.innerHTML = evt.data

                    if (evt.data.match(/error/gi)){
			dataelement.className += " alert";
                    }

		    if (evt.data.match(/warning/g)){
			dataelement.className += " warning";
                    }


		    if ( data.childNodes.length > 200 ){
			data.removeChild(data.childNodes[0]);
                    }
                    data.appendChild(dataelement);
		    data.scrollTop = data.scrollHeight;
                }
            })();
    </script>
  </body>
</html>
`
